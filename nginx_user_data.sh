#!/bin/bash
# Get instance ID
echo '######################### Getting EC2 instance ID #########################'
INSTANCE_ID=`/usr/bin/curl -s http://169.254.169.254/latest/meta-data/instance-id`
echo "######################### EC2 instance ID: $INSTANCE_ID"
# Allocate elastic IP
echo '######################### Allocate elastic IP #########################'
aws ec2 associate-address --instance-id "$INSTANCE_ID" --allocation-id=eipalloc-0bdecdc4f2fe091b0 --region us-east-1
# Add EPEL repo
echo '######################### Install epel repo #########################'
amazon-linux-extras install -y epel
# Install Nginx
echo '######################### Installing nginx #########################'
yum install -y nginx
# Install git
echo '######################### Installing git #########################'
yum install -y git
# Clone nginx repo
echo '######################### Cloning nginx config repo #########################'
git clone https://gitlab.com/tmedanovic/nginx-test.git
# Copy nginx config
echo '######################### Apply cloned nginx config #########################'
rsync -r -v -I --links nginx-test/* /etc/nginx
# Create a common ACME-challenge directory (for Let's Encrypt):
echo '######################### Create lets encrypt dirs #########################'
mkdir -p /var/www/_letsencrypt
chown nginx /var/www/_letsencrypt
# Comment out SSL related directives in configuration:
echo '######################### Temp comment out ssl lines until we obtain certs #########################'
sed -i -r 's/(listen .*443)/\1;#/g; s/(ssl_(certificate|certificate_key|trusted_certificate) )/#;#\1/g' /etc/nginx/sites-available/ec2.stap.xyz.conf /etc/nginx/sites-available/ec3.stap.xyz.conf
# Start nginx
echo '######################### Start nginx #########################'
service nginx start
# Reload nginx
echo '######################### Reload nginx #########################'
nginx -t && systemctl reload nginx
# Install certbot
echo '######################### Install certbot #########################'
yum install -y certbot
# Get certificate from let's encrypt
echo '######################### Get cert from letsencrypt #########################'
certbot certonly --webroot -d ec2.stap.xyz --email info@ec2.stap.xyz -w /var/www/_letsencrypt -n --agree-tos --force-renewal
certbot certonly --webroot -d ec3.stap.xyz --email info@ec3.stap.xyz -w /var/www/_letsencrypt -n --agree-tos --force-renewal
# Uncomment SSL related directives in configuration:
echo '######################### Uncomment nginx SSL config #########################'
sed -i -r 's/#?;#//g' /etc/nginx/sites-available/ec2.stap.xyz.conf /etc/nginx/sites-available/ec3.stap.xyz.conf
# Change premissions to certificates
echo '######################### Set letsencrypt premissions #########################'
chmod -R 755 /etc/letsencrypt/live/
chmod -R 755 /etc/letsencrypt/archive/
# Reload nginx
echo '######################### Reload nginx #########################'
nginx -t && systemctl reload nginx
# Configure Certbot to reload NGINX after success renew:
echo '######################### Configure Certbot to reload NGINX after success renew #########################'
echo -e '#!/bin/bash\n/usr/sbin/nginx -t && /usr/bin/systemctl reload nginx' | tee /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh
chmod a+x /etc/letsencrypt/renewal-hooks/post/nginx-reload.sh
# Setup automatic cert renewal
echo '######################### Setup automatic cert renewal #########################'
(crontab -l 2>/dev/null; echo "00 05 * * * certbot renew >> /var/log/certs.log 2>&1") | crontab -